FROM python:latest

COPY . .
RUN pip install -r requirements.txt

# ENV USERDB=postgres
# ENV PASSWORDDB=1234
# ENV HOST=172.18.0.2
# ENV DBNAME=sro_ontology

ADD start.sh /
RUN chmod +x /start.sh

CMD ["/start.sh"]

# CMD ["python", "create_db.py"]
# CMD ["python", "wize_script.py"]
